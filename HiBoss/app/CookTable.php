<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
// use TCG\Voyager\Traits\Translatable;

class CookTable extends Model
{
    // use Translatable;
    // protected $translatable = ['name', 'description'];
    use Sluggable;
    public function  idStyleMenu(){
        return $this->belongsTo(StyleMenu::class,'id_style_menu','id');
    }
    public function sluggable()
      {
          return [
              'slug' => [
                  'source' => 'name'
              ]
          ];
      }
 
}
