<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{

    public function  idStyleImage(){
        return $this->belongsTo(StyleImage::class,'id_style_image','id');
    }

}
