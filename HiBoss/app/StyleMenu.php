<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class StyleMenu extends Model
{
    use Sluggable;
    public function cook_table(){
        return $this->hasMany(CookTable::class,'id_style_menu','id');
    }
    public function sluggable()
      {
          return [
              'slug' => [
                  'source' => 'name'
              ]
          ];
      }
}
