<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
class StyleImage extends Model
{
    use Sluggable;
    public function images(){
        return $this->hasMany(Image::class,'id_style_image','id');
      }
    public function sluggable()
      {
          return [
              'slug' => [
                  'source' => 'name'
              ]
          ];
      }
}
