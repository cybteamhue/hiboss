<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Infomation extends Model
{
    use Sluggable;
    protected $fillable = ['name','email','phone','address','facebook','content','contents','image','slug'];

    public function sluggable()
      {
          return [
              'slug' => [
                  'source' => 'name'
              ]
          ];
      }
}
