<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BookTable;
use App\Infomation;
use Illuminate\Support\Facades\Mail;

class BookTableController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $book_table = BookTable::create($request->all());
        $restaurants = Infomation::get()->first();
        $data=  ['name_guest'=>$request->name,
                'email_guest'=>$request->email,
                'phone_guest'=>$request->phone,
                'day'=>$request->date,
                'hour'=>$request->hour,
                'name'=>$restaurants->name,
                'address'=>$restaurants->address,
                'phone'=>$restaurants->phone,
                ];
        Mail::send('shared.action.sendmail',$data,function($message){
            $mails = BookTable::all()->last();
            $restaurants = Infomation::get()->first();
            $message->from('hdlevent26@gmail.com',$restaurants->name);
            $message->to($mails->email)->subject('Xác nhận đặt bàn');
        });
        return redirect()->back()->with('message','Cảm ơn bạn đã đặt bàn, xin vui lòng kiểm tra lại email. Chúng tôi sẽ liên hệ bạn trong thời gian sớm nhất');
    }
}
