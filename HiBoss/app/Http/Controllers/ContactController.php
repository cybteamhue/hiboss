<?php

namespace App\Http\Controllers;

use App\Promotion;
use App\StyleImage;
use Illuminate\Http\Request;
use App\Infomation;
use App\Image;
use App\StyleMenu;


class ContactController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request,StyleImage $styleImageModel,Image $imageModel,StyleMenu $style_menuModel, Infomation $informationModel)
    {
        $infomations = $informationModel->get()->first();
        $gallerys = $imageModel->limit(6)->get();
        $style_menus = $style_menuModel->get();
        $type_images = $styleImageModel->get();
        $view = compact('infomations','gallerys','style_menus','type_images');
        return view('pages.contact',$view);
    }

    public function contact_message(Request $request)
    {

    }
}
