<?php

namespace App\Http\Controllers;

use App\Image;
use App\Infomation;
use App\Promotion;
use App\StyleImage;
use App\StyleMenu;
use Illuminate\Http\Request;

class MenuListController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request,StyleImage $styleImageModel,Image $imageModel, StyleMenu $style_menuModel, Infomation $informationModel)
    {
        $infomations = $informationModel->get()->first();
        $gallerys = $imageModel->limit(6)->get();
        $type_images = $styleImageModel->get();
        $style_menus = $style_menuModel->with('cook_table')->get();
        $view = compact('style_menus','infomations','gallerys','type_images');
        return view('pages.menu_list_02',$view);
    }
}
