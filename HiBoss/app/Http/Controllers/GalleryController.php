<?php

namespace App\Http\Controllers;

use App\Image;
use App\Promotion;
use Illuminate\Http\Request;
use App\Infomation;
use App\StyleImage;
use App\StyleMenu;
use TCG\Voyager\Models\Category;

class GalleryController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke($slug,StyleImage $styleImageModel,StyleMenu $style_menuModel,Infomation $informationModel,Image $imageModel)
    {
        $infomations = $informationModel->get()->first();
        $type_images = $styleImageModel->get();
        $type_image = $styleImageModel->where('slug',$slug)->firstOrFail();
        $type_image->load(['images' => function ($q) {
            $q->orderBy('id', 'desc')->paginate(20);
        }]);
        $style_menus = $style_menuModel->get();
        $gallerys = $imageModel->limit(6)->get();
        $view = compact('infomations','type_image','style_menus','type_images','gallerys');
        return  view('pages.gallery',$view);
    }
}
