<?php

namespace App\Http\Controllers;

use App\Promotion;
use App\StyleImage;
use Illuminate\Http\Request;
use TCG\Voyager\Models\Post;
use TCG\Voyager\Models\Category;
use App\Infomation;
use App\Image;
use App\StyleMenu;



class BlogCategoryController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke($slug, Request $request,StyleImage $styleImageModel,Promotion $promotionModel,StyleMenu $style_menuModel,Image $imageModel, Post $postModel, Infomation $informationModel,Category $categoryModel)
    {
        $infomations = $informationModel->get()->first();
        $categories = $categoryModel->get();
        $categoriess = Category::where('slug',$slug)->firstOrFail();
        $categoriess->load(['posts' => function ($q) {
            $q->orderBy('created_at', 'desc')->paginate(10);
            }]);
        $categories = $categoryModel->get();
        $style_menus = $style_menuModel->get();
        $gallerys = $imageModel->limit(6)->get();
        $type_images = $styleImageModel->get();
        $view = compact('infomations','categories','categoriess','style_menus','gallerys','type_images');
        return view('pages.blog_category',$view);
    }

}
