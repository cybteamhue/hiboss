<?php

namespace App\Http\Controllers;

use App\Promotion;
use App\StyleImage;
use Illuminate\Http\Request;
use App\StyleMenu;
use App\CookTable;
use App\Infomation;
use App\Image;
use TCG\Voyager\Models\Post;
use TCG\Voyager\Models\Page;
use Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request,StyleImage $styleImageModel,Promotion $promotionModel,Page $pagesModel, StyleMenu $style_menuModel, Infomation $infomationModel, Post $postModel, Image $imageModel, CookTable $menuModel)
    {
        $infomations = $infomationModel->get()->first();
        $style_menus = $style_menuModel->with([
            'cook_table' => function ($query) {
                $query->orderBy('created_at',"ASC")->limit(20);
            }
        ])->paginate(7);

        $menus = $menuModel->orderBy('id', 'ASC')->where('id_style_menu', 5)->get();
        $news_promotion = $pagesModel->orderBy('created_at', 'desc')->limit(3)->get();
        $promotion = $promotionModel->get()->first();
        $posts = $postModel->orderBy('created_at', 'desc')->limit(3)->get();
        $wines = $menuModel->orderBy('id', 'asc')->where('id_style_menu', 7)->limit(20)->get();
        $type_images = $styleImageModel->get();
        $gallerys = $imageModel->limit(6)->get();
        $view = compact( 'style_menus','infomations', 'posts','gallerys', 'menus', 'promotion', 'wines','news_promotion','type_images');
        return view('pages.home', $view);
    }
}
//->where('date_promotion', '>', Carbon::now())
