<?php

namespace App\Http\Controllers;

use App\Promotion;
use App\StyleImage;
use Illuminate\Http\Request;
use App\Infomation;
use App\StyleMenu;
use App\Image;

class AboutController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request,StyleImage $styleImageModel,Image $imageModel,Infomation $informationModel,StyleMenu $style_menuModel)
    {
        $infomations = $informationModel->get()->first();
        $style_menus = $style_menuModel->get();
        $gallerys = $imageModel->limit(6)->get();
        $type_images = $styleImageModel->get();
        $view = compact('infomations','style_menus','gallerys','type_images');
        return  view('pages.about_us',$view);
    }
}
