<?php

namespace App\Http\Controllers;

use App\Promotion;
use App\StyleImage;
use Illuminate\Http\Request;
use TCG\Voyager\Models\Post;
use TCG\Voyager\Models\Page;
use TCG\Voyager\Models\Category;
use App\Infomation;
use App\Image;
use App\StyleMenu;

class BlogController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request,StyleImage $styleImageModel,Promotion $promotionModel,StyleMenu $style_menuModel,Image $imageModel, Post $postModel, Infomation $informationModel,Category $categoryModel)
    {
        $infomations = $informationModel->get()->first();
        $posts = $postModel->orderBy('created_at', 'DESC')->paginate(10);
        $categories = $categoryModel->get();
        $gallerys = $imageModel->limit(6)->get();
        $style_menus = $style_menuModel->get();
        $type_images = $styleImageModel->get();
        $view = compact('posts','infomations','categories','gallerys','style_menus','type_images');
        return view('pages.blog',$view);
    }
    public function detail_blog($slug,StyleMenu $style_menuModel,Image $imageModel,StyleImage $styleImageModel)
    {
        $infomations = Infomation::get()->first();
        $detail = Post::where('slug',$slug)->firstOrFail();
        $categories = Category::get();
        $style_menus = $style_menuModel->get();
        $type_images = $styleImageModel->get();
        $gallerys = $imageModel->limit(6)->get();
        $viewData = compact('detail','infomations','categories','style_menus','gallerys','type_images');
        return view('pages.detail_blog',$viewData);
    }
    public function search_blog(Request $request,StyleMenu $style_menuModel,Image $imageModel)
    {
        $infomations = Infomation::get()->first();
        $categories = Category::get();
        $style_menus = $style_menuModel->get();
        $gallerys = $imageModel->limit(6)->get();
        $search_blog = Post::where('title','like','%'.$request->keyword.'%')
                        ->paginate(8);
        $view = compact('search_blog','infomations','categories','style_menus','gallerys');
        return view('pages.search_blog',$view);
    }
    public function news_promotion($slug,StyleMenu $style_menuModel,Image $imageModel)
    {
        $infomations = Infomation::get()->first();
        $detail_promotion = Page::where('slug',$slug)->firstOrFail();
        $style_menus = $style_menuModel->get();
        $gallerys = $imageModel->limit(6)->get();
        $viewData = compact('detail_promotion','infomations','style_menus','gallerys');
        return view('pages.news_promotion',$viewData);
    }
}
