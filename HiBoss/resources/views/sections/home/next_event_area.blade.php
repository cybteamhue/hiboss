
@if(count($news_promotion)>0)
<section class="next_event_area" id="tin-khuyen-mai">
    <div class="container">
        <div class="s_white_red_title">
            <h2>SỰ KIỆN</h2>
        </div>
        <div class="next_event_slider">
            @foreach ($news_promotion as $promotion)
                <div class="item">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="left_event_img">
                                <img src="{{ Voyager::image( method_exists($promotion, 'thumbnail') ? $promotion->thumbnail('cropped') : $promotion->image ) }}"
                                    height="331px" alt="{{$promotion->title}}">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="right_event_text">
                                <a href="#">
                                    <h3 class="promotion_event">{{$promotion->title}}</h3>
                                </a>
                                <p>{{$promotion->excerpt}}... <a href="{{route('news_promotion',$promotion->slug)}}">Xem chi
                                        tiết</a></p>
                                <div class="event_shedule">
                                    <time datetime="{{$promotion->date_promotion}}"></time>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>
@endif
