<section class="service_area main-course">
    <div class="container">
        <div class="row">
            @foreach ($main_menu as $main)
            <div class="col-md-3 col-sm-6">
                <div class="service_item">
                    <img src="{{ Voyager::image( method_exists($main, 'thumbnail') ? $main->thumbnail('cropped') : $main->image ) }}"
                        width="128" height="128" alt="{{$main->name}}">
                    <div class="menu_service">
                        <h3>{{$main->name}}</h3>
                        @if (App::isLocale('en'))
                        <p>{!!$main->description_eng!!}</p>
                        @else
                        <p>{!!$main->description!!}</p>
                        @endif
                    </div>
                    <a class="read_mor_btn" href="{{route('menu_list')}}">{{ __("more")}}</a></a>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>
