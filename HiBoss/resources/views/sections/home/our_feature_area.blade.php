<section class="our_feature_area">
    <div class="container">
        <div class="s_black_title">
            <h3>{{ __("Main Course")}}</h3>
        </div>
        <div class="feature_slider">
            @foreach ($menus as $menu)
            <div class="item">
                <div class="feature_item">
                    <div class="feature_item_inner ">
                        <img src="{{ Voyager::image( method_exists($menu, 'thumbnail') ? $menu->thumbnail('cropped') : $menu->image ) }}"
                            width="100%" height="100%" alt="{{$menu->name}}">
                    </div>
                    <div class="title_text">
                        @if (App::isLocale('en'))
                        <div class="feature_left"><span>{{$menu->name_eng}}</span></div>
                        @else
                        <div class="feature_left"><span>{{$menu->name}}</span></div>
                        @endif
                        <div class="restaurant_feature_dots"></div>
                        <div class="feature_right">{{$menu->price}}</div>
                    </div>
                </div>
            </div>
            @endforeach

        </div>
    </div>
</section>


