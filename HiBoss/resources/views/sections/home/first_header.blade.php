<div class="first_header">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="header_contact_details">
                    <a><i class="fa fa-phone"></i>{{$infomations->phone}}</a>
                    @if (App::isLocale('en'))
                        <a><i class="fa fa-map-marker"></i>{{$infomations->address_eng}}</a>
                    @else
                        <a><i class="fa fa-map-marker"></i>{{$infomations->address}}</a>
                    @endif
                </div>
            </div>
            <div class="col-md-4">
                <div class="event_btn_inner">
                    <a class="event_btn" href="#dat-ban"><i class="fa fa-table"
                                                            aria-hidden="true"></i>{{ __("book_a_table")}}</a>
                    <a class="event_btn" href="#tin-khuyen-mai"><i class="fa fa-calendar"
                                                                   aria-hidden="true"></i>{{ __("news_promotion")}}
                    </a>
                </div>
            </div>
            <div class="col-md-4 hidden-lg visible-md-* visible-sm-* visible-xs-*">
                <div class="header_social">
                    <ul>
                        <li><a href="lang/en"><img src="{{asset('restaurant/img/logo/us.png')}}" width="30px"
                                                   height="20px">
                            </a></li>
                        <li><a href="lang/vi"><img src="{{asset('restaurant/img/logo/vn.jpg')}}" width="30px"
                                                   height="20px">
                            </a></li>
                    </ul>

                </div>
            </div>
        </div>
    </div>
</div>
