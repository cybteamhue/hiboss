<section class="booking_table_area"  id="dat-ban">
    <div class="container">
        <div class="s_white_title">
            <h2>{{ __("booknow")}}</h2>
        </div>
        <div class="row">
            <form id="subscriber-form" action="javascript:void(0)" method="POST">
                @csrf
                <div class="col-sm-2">
                    <div class="input-append">
                        <input size="16" type="text" value="" name="name" placeholder="{{ __("name")}}" required>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="input-append">
                        <input size="16" type="email" value="" name="email" placeholder="Email" required>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="input-append">
                        <input size="16" type="text" value="" name="phone" placeholder="{{ __("phone")}}" required>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="input-append date form_datetime">
                        <input size="16" type="text" value="" name="date" readonly placeholder="{{ __("day")}}">
                        <span class="add-on"><i class="icon-th"></i></span>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="input-append date form_time">
                        <input size="16" type="text" value="" name="hour" readonly placeholder="{{ __("hours")}}" required>
                        <span class="add-on"><i class="icon-th"></i></span>
                    </div>
                </div>
                <div class="col-sm-2">
                    <button type="submit"id="send_form"  class="btn btn-default submit_btn">{{ __("booknow")}}</button>
                </div>
            </form>
        </div>
    </div>
    @include('shared.action.book_table')
</section>
