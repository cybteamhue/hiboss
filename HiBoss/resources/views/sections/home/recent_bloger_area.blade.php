<section class="recent_bloger_area">
    <div class="container">
        <div class="s_black_title">
            <h2>TIN TỨC</h2>
        </div>
        <div class="row">
            @foreach ($posts as $post)
            <div class="col-md-4">
                <div class="recent_blog_item">
                    <div class="blog_img">
                        <a href="{{route('detail_blog',$post->slug)}}"><img src="{{ Voyager::image( method_exists($post, 'thumbnail') ? $post->thumbnail('cropped') : $post->image ) }}"
                            width="360" height="270" alt="{{$post->title}}">
                        </a>
                    </div>
                    <div class="recent_blog_text">
                        <div class="recent_blog_text_inner">
                            <h6><i class="fa fa-clock-o"></i> {{$post->created_at->format('d-m-Y')}}</h6>
                            <a href="{{route('detail_blog',$post->slug)}}" class="detail_blog">
                                <h5>{{$post->title}}</h5>
                            </a>
                            <p>{!! $post->excerpt !!}</p>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>
