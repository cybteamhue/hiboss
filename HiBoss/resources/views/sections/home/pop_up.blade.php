<div class="container">
    <button type="button" class="btn btn-info btn-lg" style="display: none" data-toggle="modal" id="popup"
            data-target="#myModal">Open Modal
    </button>
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header pop-container">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title pop-fixer">{{$promotion->title}}</h4>
                    <p class="ig_message">{!! $promotion->content !!} </p>
                    <div class="text-center">
                        <a class="more_menu_pop_up button" href="{{ $promotion->link }}" target="_blank">XEM THÊM</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
