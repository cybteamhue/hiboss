<section class="our_chefs_area">
        <div class="container">
            <div class="s_black_title">
                <h3>THE</h3>
                <h2>WINES</h2>
            </div>
            <div class="chefs_slider_active">
                    @foreach ($wines as $wine)
                    <div class="item">
                        <div class="chef_item_inner">
                            <div class="chef_img">
                                <img src="{{ Voyager::image( method_exists($wine, 'thumbnail') ? $wine->thumbnail('cropped') : $wine->image ) }}"
                                width="100%" height="100%" alt="{{$wine->name}}">
                                <div class="chef_hover wines">
                                    @if (App::isLocale('en'))
                                        <h5>{{$wine->name_eng}}</h5>
                                        <p>{!!$wine->description_eng!!}</p>
                                    @else
                                        <h5>{{$wine->name}}</h5>
                                        <p>{!!$wine->description!!}</p>
                                    @endif

                                </div>
                            </div>
                            <div class="chef_name">
                                <div class="name_chef_text">
                                    @if (App::isLocale('en'))
                                        <h3>{{$wine->name_eng}}</h3>
                                        <h4>{{$wine->origin}}</h4>
                                        <h4>{{$wine->price}}</h4>
                                    @else
                                        <h3>{{$wine->name}}</h3>
                                        <h4>{{$wine->origin}}</h4>
                                        <h4>{{$wine->price}}</h4>
                                    @endif
                                </div>
                                <ul>
                                    <li>{{$wine->note}}</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
