    <div class="col-md-8">
        <div class="row m0">
            <div class="blog_details_inner">
                <div class="blog_details_content">
                    <h3>{{$detail->title}}</h3>
                    <h4><i class="fa fa-clock-o"></i> {{$detail->created_at->format('d-m-Y')}}</h4>
                    <p>{!! $detail->body !!}</p>
                </div>
            </div>
        </div>
    </div>
