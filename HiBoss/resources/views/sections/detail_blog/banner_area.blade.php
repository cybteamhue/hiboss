    <section class="banner_area">
        <div class="container">
            <div class="banner_content">
                <h4>Blog Details</h4>
                <a href="{{route('home')}}">Home</a>
                <a href="{{route('blog')}}">Blog</a>
                <a class="active" href="blog-details.html">Details</a>
            </div>
        </div>
    </section>
