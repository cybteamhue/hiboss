<div class="col-md-8">
    <div class="row">

        @foreach($categoriess->posts as $post)
            <article class="blog_list_item row m0">
                <div class="col-md-5 image_blog">
                    <div class="blog_list_img">
                        <a href="{{ route('detail_blog',$post->slug) }}"> <img
                                src="{{ Voyager::image( method_exists($post, 'thumbnail') ? $post->thumbnail('cropped') : $post->image ) }}"
                                width="100%" height="240" alt="{{$post->title}}"></a>
                    </div>
                </div>
                <div class="col-md-7 content-blog">
                    <div class="blog_list_content">
                        <div class="blog">
                            <a href="{{ route('detail_blog',$post->slug) }}"><h4>{{$post->title}}</h4></a>
                            <h6><i class="fa fa-clock-o"></i> {{$post->created_at->format('d-m-Y')}}</h6>
                            <p>{!! $post->excerpt !!}</p>
                        </div>
                    </div>
                </div>
            </article>
        @endforeach
    </div>
    <nav aria-label="Page navigation" class="blog_pagination">
        <ul class="pagination">
            {!! $categoriess->paginate(10) !!}
        </ul>
    </nav>
</div>
