<div class="col-md-4">
    <div class="blog_right_sidebar">
        @include('sections.blog.search')
        <aside class="right_widget category_widget">
            <div class="sidebar_title">
                <h3>Thể Loại</h3>
            </div>
            <ul>
                @foreach ($categories as $category)
                <li><a href="{{ route('blogs',$category->slug) }}"><i
                            class="fa fa-chevron-right"></i>{{$category->name}} ({{ count($category->posts) }})</a></li>
                @endforeach
            </ul>
        </aside>
        <aside class="right_widget calender_widget">
            <div class="sidebar_title">
                <h3>Calendar</h3>
            </div>
            <div id="my-calendar"></div>
        </aside>
    </div>
</div>
