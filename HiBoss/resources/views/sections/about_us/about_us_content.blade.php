<section class="about_us_content">
    <div class="container">
        <div class="row about_inner_item">
            <div class="col-md-6">
                <div class="about_left_content">
                    <h4>{{$infomations->name}}</h4>
                    @if (App::isLocale('en'))
                        <p class="hiboss">{!!$infomations->contents_eng!!}</p>
                    @else
                        <p class="hiboss">{!!$infomations->contents!!}</p>
                    @endif
                    <div class="row conten_hi_boss">
                        @foreach ($style_menus as $style_menu)
                            <div class="col-md-4 "><i
                                    class="fa fa-arrow-circle-right style_menu"></i> {{$style_menu->name}}
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="about_right_image">
                    <iframe
                        src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fnhahangruouhiboss%2Fvideos%2F959294007765374%2F&show_text=0&width=560"
                        width="100%" height="315" style="border:none;overflow:hidden" scrolling="no"
                        frameborder="0"
                        allowTransparency="true" allowFullScreen="true"></iframe>
                </div>
            </div>
            <div class="col-md-12">
                <div class="about_single_content">
                    @if (App::isLocale('en'))
                        <p>{!!$infomations->content_eng!!}</p>
                    @else
                        <p>{!!$infomations->content!!}</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
</section>
