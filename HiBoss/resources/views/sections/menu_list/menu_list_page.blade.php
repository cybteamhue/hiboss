<section class="most_popular_item_area menu_list_page">
    <div class="container">
        <div class="popular_filter">
            <ul>
                <li class="active" data-filter="*"><a href="">All</a></li>
                @foreach ($style_menus as $style_menu)
                    <li data-filter=".{{$style_menu->id}}"><a href="">{{$style_menu->name}}</a></li>
                @endforeach
            </ul>
        </div>
        <div class="p_recype_item_main item-menu">
            <div class="row p_recype_item_active">
                @foreach ($style_menus as $style_menu)
                    @foreach ($style_menu->cook_table as $menu)
                        <div class="col-md-6 col-sm-6 pix-code-grid  {{$menu->id_style_menu}}">
                            <div class="row">
                                <div class="col-md-6 col-sm-6 ">
                                    <img
                                        src="{{ Voyager::image( method_exists($menu, 'thumbnail') ? $menu->thumbnail('cropped') : $menu->image ) }}"
                                        width="100%" height="100%" alt="{{$menu->name}}">
                                </div>
                                <div class="col-md-6 hi_boss_menu_title">
                                    @if (App::isLocale('en'))
                                        <h3 >{{$menu->name_eng}}</h3>
                                    @else
                                        <h3>{{$menu->name}}</h3>
                                    @endif
                                    <h4 class="price-title">{{$menu->price}}</h4>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endforeach
            </div>
        </div>
    </div>
</section>
