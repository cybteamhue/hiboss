<div class="filters_listing version_2 ">
    <div class="container">
        <ul class="clearfix">          
            <li>
                <a class="btn_map" data-toggle="collapse" href="#collapseMap" aria-expanded="false" aria-controls="collapseMap" data-text-swap="Ẩn bản đồ" data-text-original="Xem bản đồ">Xem bản đồ</a>
            </li>           
        </ul>
    </div>
    <!-- /container -->
</div>
<div class="collapse" id="collapseMap">
    <div id="map" class="map">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d956.4701282949069!2d107.59539140856182!3d16.481586859043333!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3141a0df8c2a9a0d%3A0xa4d83e0d0debd33!2zMTQ3IE5ndXnhu4VuIFNpbmggQ3VuZywgVuG7uSBE4bqhLCBUaMOgbmggcGjhu5EgSHXhur8sIFRo4burYSBUaGnDqm4gSHXhur8sIFZp4buHdCBOYW0!5e0!3m2!1svi!2s!4v1572010815714!5m2!1svi!2s" width="100%" height="530" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
    </div>
</div>