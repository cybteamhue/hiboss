<section class="contact_area">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="contact_details">
                    <h3 class="contact_title">THÔNG TIN VỀ CHÚNG TÔI</h3>
                    <div class="media">
                        <div class="media-left">
                            <i class="fa fa-map-marker"></i>
                        </div>
                        <div class="media-body">
                            <h4>Addtress</h4>
                            <h5>{{$infomations->address}}</h5>
                        </div>
                    </div>
                    <div class="media">
                        <div class="media-left">
                            <i class="fa fa-phone"></i>
                        </div>
                        <div class="media-body">
                            <h4>Phone</h4>
                            <h5>{{$infomations->phone}}</h5>
                        </div>
                    </div>
                    <div class="media">
                        <div class="media-left">
                            <i class="fa fa-facebook"></i>
                        </div>
                        <div class="media-body">
                            <h4>Facebook</h4>
                            <h5><a href="{{$infomations->facebook}}" target="_blank">{{$infomations->facebook}}</a></h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row contact_form_area">
                    <h3 class="contact_title">LIÊN HỆ VỚI CHÚNG TÔI</h3>
                    <h4 class="contact_hi"> Vui lòng để lại câu hỏi, Hi BOSS sẽ liên hệ với bạn</h4>
                    @if(session('message'))
                    <div class="alert alert-success contact_hiBoss">
                        <strong>{{session('message')}}</strong>
                    </div>
                    @endif
                <form action="{{route('contact')}}" method="POST" id="contactForm">
                        @csrf
                        <div class="form-group col-md-12">
                            <input type="text" class="form-control" id="name" name="name" placeholder="Họ và tên">
                        </div>
                        <div class="form-group col-md-12">
                            <input type="email" class="form-control" id="email" name="email" placeholder="Email*">
                        </div>
                        <div class="form-group col-md-12">
                            <textarea class="form-control" id="message" name="message"
                                placeholder="Viết tin nhắn"></textarea>
                        </div>
                        <div class="form-group col-md-12">
                            <button class="btn btn-default submit_btn" type="submit">Gửi tin nhắn</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
