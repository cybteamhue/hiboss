<section class="most_popular_item_area menu_list_page">
    <div class="container">
        <div class="popular_filter">
            <ul>
                <li class="active" data-filter="*"><a href="">All</a></li>
                @foreach ($style_menus as $style_menu)
                    <li data-filter=".{{$style_menu->id}}"><a href="">{{$style_menu->name}}</a></li>
                @endforeach
            </ul>
        </div>
        <div class="p_recype_item_main">
            <div class="row p_recype_item_active">
                @foreach ($style_menus as $style_menu)
                    @foreach ($style_menu->cook_table as $menu)
                        <div class="col-md-6 media_hi_boss {{$menu->id_style_menu}}">
                            <div class="media">
                                <div class="hi_boss_menu">
                                    @if (App::isLocale('en'))
                                  <span class="menu-list">  {{$menu->name_eng}}   </span>
                                    @else
                                        <span  class="menu-list">{{$menu->name}}</span>
                                    @endif
                                    <div class="menu-list__dotted"></div>
                                    <div class="menu-list__item">{{$menu->price}}</div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endforeach
            </div>
        </div>
    </div>
</section>
