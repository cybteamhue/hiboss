<section class="our_gallery_area">
    <div class="container">
        <div class="row our_gallery_ms_inner">
            @foreach ( $type_image->images as $image)
                <div class="col-md-4 col-sm-6">
                    <div class="our_gallery_item">
                        <img
                            src="{{ Voyager::image( method_exists($image, 'thumbnail') ? $image->thumbnail('cropped') : $image->image ) }}"
                            width="360" height="266" alt="{{$image->name}}">
                        <div class="our_gallery_hover">
                            <a href="#"><h5>{{$image->name}}</h5></a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <nav aria-label="Page navigation" class="blog_pagination">
            <ul class="pagination">
                {!! $type_image->paginate(20) !!}
            </ul>
        </nav>
    </div>
</section>
