<footer class="footer_area">
    <div class="footer_widget_area">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <aside class="f_widget about_widget">
                        <div class="f_w_title infor">
                            <h4>{{$infomations->name}}</h4>
                        </div>
                        @if (App::isLocale('en'))
                            <p>{!!substr($infomations->contents_eng,0,400)!!}</p>
                        @else
                            <p>{!!substr($infomations->contents,0,400)!!}</p>
                        @endif
                        <ul>
                            <li><a href="{{$infomations->facebook}}"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </aside>
                </div>
                <div class="col-md-3">
                    <aside class="f_widget contact_widget">
                        <div class="f_w_title">
                            <h4>{{ __("contact")}}</h4>
                        </div>
                        <p>{{ __("Have questions, comments or just want to say hello")}}</p>
                        <ul>
                            <li><a href="#"><i class="fa fa-phone"></i>{{$infomations->phone}}</a></li>
                            @if (App::isLocale('en'))
                                <li><a href="#"><i class="fa fa-map-marker"></i>{{$infomations->address_eng}}</a></li>
                            @else
                                <li><a href="#"><i class="fa fa-map-marker"></i>{{$infomations->address}}</a></li>
                            @endif
                        </ul>
                    </aside>
                </div>
                <div class="col-md-3">
                    <aside class="f_widget twitter_widget">
                        <div class="f_w_title">
                            <h4>Category Menu</h4>
                        </div>
                        <div class="row">
                            @foreach ($style_menus as $style_menu)
                                <div class="col-md-6 f_w_style ">
                                    <i class="fa fa-arrow-circle-right style_menu"></i> {{$style_menu->name}}
                                </div>
                            @endforeach
                        </div>
                    </aside>
                </div>
                <div class="col-md-3">
                    <aside class="f_widget gallery_widget">
                        <div class="f_w_title">
                            <h4>Gallery</h4>
                        </div>
                        <ul>
                            @foreach ($gallerys as $gallery)
                                <li><a href="#"><img
                                            src="{{ Voyager::image( method_exists($gallery, 'thumbnail') ? $gallery->thumbnail('cropped') : $gallery->image ) }}"
                                            alt="{{$gallery->name}}"><i class="fa fa-search"></i></a>
                                </li>
                            @endforeach
                        </ul>
                    </aside>
                </div>
            </div>
        </div>
    </div>
    <div id="toTop"></div>
    @include('shared.action.chat')
    @include('shared.action.phone')

    <div class="copy_right_area">
        <div class="container">
            <div class="pull-left">
                <h5>
                    <p>
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        Copyright &copy;<script>
                            document.write(new Date().getFullYear());

                        </script>
                        All rights reserved | This template is made with <i class="fa fa-heart-o"
                                                                            aria-hidden="true"></i> by <a
                            href="https://colorlib.com" target="_blank">Colorlib</a>
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    </p>
                </h5>
            </div>
            <div class="pull-right">
                <ul class="nav navbar-nav navbar-right">
                    <li class="active"><a href="#">Home</a></li>
                    <li><a href="#">About Us</a></li>
                    <li><a href="#">Menu</a></li>
                    <li><a href="#">Gallery</a></li>
                    <li><a href="#">Reservation</a></li>
                    <li><a href="#">News</a></li>
                    <li><a href="#">Contact Us</a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>
