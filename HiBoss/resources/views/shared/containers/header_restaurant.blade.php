<div>
    @include('sections.home.loader')
    @include('sections.home.first_header')   
    @include('shared.headers.default')
    @yield('content')
    @include('shared.footers.default')
</div>