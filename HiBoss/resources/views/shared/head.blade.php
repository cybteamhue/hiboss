
<head>
    <base href="{{ asset('') }}">

    <title> @yield('title')</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Favicons-->
    {{-- <link href="{{ Voyager::image( method_exists($information, 'thumbnail') ? $information->thumbnail('cropped') : $information->logo ) }}" rel="apple-touch-icon">
    <link href="{{ Voyager::image( method_exists($information, 'thumbnail') ? $information->thumbnail('cropped') : $information->logo ) }}" rel="icon"> --}}
    <link rel="shortcut icon" href="restaurant/img/favicon.png" type="image/x-icon">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8">

    <!-- Font -->
    <link href="restaurant/vendors/material-icon/css/materialdesignicons.min.css" rel="stylesheet">
    <link href="restaurant/css/font-awesome.min.css" rel="stylesheet">
    <link href="restaurant/vendors/linears-icon/style.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="restaurant/css/bootstrap.min.css" rel="stylesheet">

    <!-- Rev slider css -->
    <link href="restaurant/vendors/revolution/css/settings.css" rel="stylesheet">
    <link href="restaurant/vendors/revolution/css/layers.css" rel="stylesheet">
    <link href="restaurant/vendors/revolution/css/navigation.css" rel="stylesheet">

    <!-- Extra plugin css -->
    <link href="restaurant/vendors/bootstrap-selector/bootstrap-select.css" rel="stylesheet">
    <link href="restaurant/vendors/bootatrap-date-time/bootstrap-datetimepicker.min.css" rel="stylesheet">
    <link href="restaurant/vendors/owl-carousel/assets/owl.carousel.css" rel="stylesheet">

    <link href="restaurant/css/style.css" rel="stylesheet">
    <link href="restaurant/css/responsive.css" rel="stylesheet">
    <link href="restaurant/css/chat.css" rel="stylesheet">
    <link href="restaurant/css/phone.css" rel="stylesheet">

    @stack('head')
</head>
