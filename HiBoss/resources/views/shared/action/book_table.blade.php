<button style="display:none" id="config" type="button" class="btn btn-primary btn-lg" data-toggle="modal"
    data-target="#modelId">
    Launch
</button>

<!-- Modal -->
<div class="modal fade" style="top:15%" id="modelId" tabindex="-1" role="dialog" aria-labelledby="modelTitleId"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="border:none">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" style="text-align:center" id="confirm">
                <div class="icon icon--order-success svg add_bottom_15">
                    <svg xmlns="http://www.w3.org/2000/svg" width="72" height="72">
                        <g fill="none" stroke="#8EC343" stroke-width="2">
                            <circle cx="36" cy="36" r="35"
                                style="stroke-dasharray:240px, 240px; stroke-dashoffset: 480px;"></circle>
                            <path d="M17.417,37.778l9.93,9.909l25.444-25.393"
                                style="stroke-dasharray:50px, 50px; stroke-dashoffset: 0px;"></path>
                        </g>
                    </svg>
                </div>
                <h2 class="ajax_book">BẠN ĐÃ ĐẶT BÀN THÀNH CÔNG</h2>
                <h3>Vui lòng kiểm tra lại email của bạn !</h3>
            </div>
        </div>
    </div>
</div>
