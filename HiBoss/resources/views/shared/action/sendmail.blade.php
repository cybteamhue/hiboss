<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Xác nhận đặt phòng</title>

</head>

<body>

    <section style="background-color: #EEEEEE; ">
        <h4>XÁC NHẬN ĐẶT BÀN</h4>
        <p>Xin kính chào Quý khách!</p>
        <p>Cảm ơn Quý khách đã đặt bàn tại tại nhà hàng {{$name}}.</p>
        <p>Quý khách vui lòng kiểm tra và xác nhận thông tin đặt bàn ở bên dưới</p>
        <ul>
            <li style="margin-left: 15px;">Họ và tên (Contact person): {{ $name_guest }}</li>
            <li style="margin-left: 15px;">Điện thoại (Tell): {{ $phone_guest }}</li>
            <li style="margin-left: 15px;">Email: {{ $email_guest }}</li>
            <li style="margin-left: 15px;">Đặt bàn ngày (Date):{{ $day }}</li>
            <li style="margin-left: 15px;">Giờ (Time): {{ $hour }}</li>
        </ul>
        <strong>Đây là email tự động vui lòng không phản hồi lại, vì phản hồi lại chúng tôi sẽ không tiếp nhận được
            thông tin.</strong>
        <div style="background-color: #FFCC00;">
            <div style="margin-left: 2%">
                <h4>Nếu quý khách có thắc mắc, xin vui lòng liên hệ với chúng tôi qua số điện thoại:{{$phone}} hoặc địa
                    chỉ: {{$address}}</h4>
            </div>
        </div>
    </section>

</body>

</html>
