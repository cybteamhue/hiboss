        <script src="restaurant/js/jquery-2.1.4.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="restaurant/js/bootstrap.min.js"></script>
        <!-- Rev slider js -->
        <script src="restaurant/vendors/revolution/js/jquery.themepunch.tools.min.js"></script>
        <script src="restaurant/vendors/revolution/js/jquery.themepunch.revolution.min.js"></script>
        <script src="restaurant/vendors/revolution/js/extensions/revolution.extension.video.min.js"></script>
        <script src="restaurant/vendors/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
        <script src="restaurant/vendors/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
        <script src="restaurant/vendors/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
        <script src="restaurant/vendors/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
        <!-- Extra plugin js -->
        <script src="restaurant/vendors/bootstrap-selector/bootstrap-select.js"></script>
        <script src="restaurant/vendors/bootatrap-date-time/bootstrap-datetimepicker.min.js"></script>
        <script src="restaurant/vendors/owl-carousel/owl.carousel.min.js"></script>
        <script src="restaurant/vendors/isotope/imagesloaded.pkgd.min.js"></script>
        <script src="restaurant/vendors/isotope/isotope.pkgd.min.js"></script>
        <script src="restaurant/vendors/countdown/jquery.countdown.js"></script>
        <script src="restaurant/vendors/js-calender/zabuto_calendar.min.js"></script>
        <!--gmaps Js-->
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCjCGmQ0Uq4exrzdcL6rvxywDDOvfAu6eE"></script>
        <script src="restaurant/js/gmaps.min.js"></script>

        <!--        <script src="restaurant/js/video_player.js"></script>-->
        <script src="restaurant/js/theme.js"></script>
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-152436574-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-152436574-1');
        </script>
        <script>
            setTimeout(function () {
                $('.alert-success').fadeOut('fast');
            }, 5000);

        </script>
         <script type="text/javascript">
            $(function(){

                $('#subscriber-form').submit(function(e){
                    var route = $('#subscriber-form').data('route');
                    var form_data = $(this);
                    $.ajaxSetup({
                        headers: {
                          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                      });
                    $('#send_form').html('Sending..');
                    $.ajax({
                        type:'POST',
                        url:'/dat-ban',
                        data:form_data.serialize(),
                        success:function(Response){
                            console.log(Response)
                            $('#send_form').html('ĐẶT BÀN');
                            $('#res_messages').html(Response.msg);
                            $('#config').trigger('click');
                            document.getElementById("subscriber-form").reset();
                            setTimeout(function(){
                            $('.close').trigger('click');
                            },3500);
                        }
                    });
                    e.preventDefault();
                })
            });
        </script>
        <script src="restaurant/js/pop_up_func.js"></script>
        @stack('foot')
