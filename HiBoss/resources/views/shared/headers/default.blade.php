<header class="main_menu_area">
        <nav class="navbar navbar-default">
                <div class="container">
                    @include('components.home.navbar-header')
                    @include('components.home.navbar-collapse')	
                </div>
        </nav>	
</header>