@extends('layouts.layout_restaurant')
@section('title')
{{$infomations->name}} - Tin tức khuyến mãi
@endsection
@section('content')
    <main>  
        @include('sections.news_promotion.banner_area') 
        <section class="blog_list_area">
            <div class="container">
                <div class="row">
                    @include('sections.news_promotion.news_promotion')            
                    @include('sections.news_promotion.promotion_right_sidebar') 
                </div>
            </div>
        </section>  
    </main>
@endsection