@extends('layouts.layout_restaurant')
@section('title')
{{$infomations->name}} - Chi tiết tin tức
@endsection
@push('head')
<meta name="keywords" content="{{$detail->meta_keywords}}">
<meta name="author" content="{{ $infomations->name }}">
<meta name="format-detection" content="telephone=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta property="og:locale" content="vi_VN">
<meta property="og:type" content="Business">
<meta property="og:title" content="{{ $detail->title }}">
<meta property="og:url" content="http://hibossrestaurant.com">
<link rel="canonical" href="http://hibossrestaurant.com">
<meta property="og:image" content="{{ Voyager::image( method_exists($detail, 'thumbnail') ? $detail->thumbnail('cropped') : $detail->image ) }}">
<meta property="og:image:alt" content="{{ $detail->title }}">
<meta property="og:image:width" content="819">
<meta property="og:image:height" content="1024">
<meta property="og:description" content="{{ $detail->excerpt }}">
@endpush
@section('content')
    <main>  
        @include('sections.detail_blog.banner_area') 
        <section class="blog_list_area">
            <div class="container">
                <div class="row">
                    @include('sections.detail_blog.blog_details_inner')            
                    @include('sections.blog.blog_right_sidebar') 
                </div>
            </div>
        </section>  
    </main>
@endsection