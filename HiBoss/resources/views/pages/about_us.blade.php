@extends('layouts.layout_restaurant')
@section('title')
{{$infomations->name}} - Về chúng tôi
@endsection
@section('content')
    <main>  
        @include('sections.about_us.banner_area')            
        @include('sections.about_us.about_us_content')   
    </main>
@endsection