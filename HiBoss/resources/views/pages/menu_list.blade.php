@extends('layouts.layout_restaurant')
@section('title')
{{$infomations->name}} - Menu
@endsection
@section('content')
    <main>   
        @include('sections.menu_list.banner_area')            
        @include('sections.menu_list.menu_list_page')   
    </main>
@endsection