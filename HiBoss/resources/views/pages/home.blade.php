@extends('layouts.default')
@push('head')
<meta name="keywords" content="restaurant, Hi Boss, Wins, red wine, white wine,Rượu vang, Món Âu, Sang trọng, Lê Quý Đôn, nhà hàng, nhà hàng sang trọng, Europen restaurant ">
<meta name="author" content="{{ $infomations->name }}">
<meta name="format-detection" content="telephone=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta property="og:locale" content="vi_VN">
<meta property="og:type" content="Business">
<meta property="og:title" content="{{ $infomations->name }}">
<meta property="og:url" content="http://hibossrestaurant.com">
<link rel="canonical" href="http://hibossrestaurant.com">
<meta name="description" content="{{$infomations->meta_description}}">
<meta property="og:image" content="{{ Voyager::image( method_exists($infomations, 'thumbnail') ? $infomations->thumbnail('cropped') : $infomations->image ) }}">
<meta property="og:image:alt" content="{{ $infomations->name }}">
<meta property="og:image:width" content="819">
<meta property="og:image:height" content="1024">
<meta property="og:description" content="{{$infomations->meta_description}}">
<meta property="og:locale" content="vi_VN">
<meta property="og:site_name" content="http://hibossrestaurant.com">
<meta name="geo.region" content="VN">
<meta name="copyright" content="&amp;copy;hibossrestaurant.com">
<meta name="googlebot" content="all, index, follow">
<meta name="geo.placename" content="Thừa Thiên Huế">
<meta name="geo.position" content="16.463878, 107.595714">
<meta name="doc-type" content="web page">
@endpush
@section('title')
{{$infomations->name}}
@endsection
@section('content')
    <main>
        @include('sections.home.slider_area')


        @include('sections.home.our_feature_area')
        @include('sections.home.most_popular_item_area')
        @include('sections.home.the-wines')
        @include('sections.home.booking_table_area')
{{--        @include('sections.home.next_event_area')--}}
        @include('sections.home.recent_bloger_area')
        @include('sections.home.pop_up')
    </main>

@endsection
