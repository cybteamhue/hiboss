@extends('layouts.layout_restaurant')
@section('title')
{{$infomations->name}} - Liên hệ
@endsection
@section('content')
    <main>  
        @include('sections.contact.banner_area')            
        @include('sections.contact.contact_area')   
        @include('sections.contact.mapBox')   
    </main>
@endsection