@extends('layouts.layout_restaurant')
@section('title')
{{$infomations->name}} - Gallery
@endsection
@section('content')
    <main>  
        @include('sections.gallery.banner_area')            
        @include('sections.gallery.our_gallery_area')   
    </main>
@endsection