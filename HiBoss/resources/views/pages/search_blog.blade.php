@extends('layouts.layout_restaurant')
@section('title')
{{$infomations->name}} - Tìm kiếm
@endsection
@section('content')
    <main>  
        @include('sections.blog.banner_area') 
        <section class="blog_list_area">
            <div class="container">
                <div class="row">
                    @include('sections.blog.search_blog')            
                    @include('sections.blog.blog_right_sidebar') 
                </div>
            </div>
        </section>  
    </main>
@endsection