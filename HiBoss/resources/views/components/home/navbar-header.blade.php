<div class="navbar-header">
    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
        data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand" href="{{route('home')}}"><img
            src="{{ Voyager::image( method_exists($infomations, 'thumbnail') ? $infomations->thumbnail('cropped') : $infomations->logo) }}"
            width="120" height="70" alt="{{$infomations->name}}"></a>
</div>
