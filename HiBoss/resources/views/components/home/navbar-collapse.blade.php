<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
    <ul class="nav navbar-nav navbar-right">
        <li><a href="{{ route('home') }}">{{ __("home")}}</a></li>
        <li><a href="{{ route('about_us') }}">{{ __("about")}}</a></li>
        <li><a href="{{ route('menu_list') }}">{{ __("menu")}}</a></li>
        <li class="dropdown submenu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
               aria-expanded="false">{{ __("gallery")}}<i class="fa fa-angle-down" aria-hidden="true"></i></a>
            <ul class="dropdown-menu">
                @foreach($type_images as $image)
                    <li><a href="{{route('gallery',$image->slug)}}">{{$image->name}}</a></li>
                @endforeach
            </ul>
        </li>
        <li><a href="{{ route('blog')}}">{{ __("news")}}</a></li>
        <li><a href="{{ route('contact') }}">{{ __("contact")}}</a></li>
        <li class="dropdown submenu">
            <ul class="navbar-nav">
                @php $locale = session()->get('locale'); @endphp
                <li class="nav-item dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                       aria-haspopup="true" aria-expanded="false">
                        @switch($locale)
                            @case('en')
                            <img src="{{asset('restaurant/img/logo/us.png')}}" width="30px" height="20px"> EN
                            @break
                            @default
                            <img src="{{asset('restaurant/img/logo/vn.jpg')}}" width="30px" height="20px"> VN
                        @endswitch
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a class="dropdown-item language" href="lang/en"><img
                                    src="{{asset('restaurant/img/logo/us.png')}}" width="30px" height="20px">
                                EN-ENGLISH</a></li>
                        <li><a class="dropdown-item language" href="lang/vi"><img
                                    src="{{asset('restaurant/img/logo/vn.jpg')}}" width="30px" height="20px">
                                VN-TIẾNG VIỆT</a></li>
                    </ul>
                </li>
            </ul>
        </li>
    </ul>

</div>
