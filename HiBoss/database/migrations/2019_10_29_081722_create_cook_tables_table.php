<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCookTablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cook_tables', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',191);
            $table->text('description')->nullable();
            $table->string('name_eng',191)->nullable();;
            $table->text('description_eng')->nullable();
            $table->integer('price');
            $table->string('origin',191)->nullable();
            $table->text('image');     
            $table->integer('id_style_menu')->unsigned();    
            $table->string('meta_keyword',1000);
            $table->string('mete_description',1000);
            $table->string('note',1000)->nullable();
            $table->string('slug',1000);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cook_tables');
    }
}
