<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConnectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("cook_tables", function (Blueprint $table) {
            $table->foreign('id_style_menu')->references('id')->on('style_menus')->onDelete('cascade');
        });
        Schema::table("images", function (Blueprint $table) {
            $table->foreign('id_style_image')->references('id')->on('style_images')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('connects');
    }
}
