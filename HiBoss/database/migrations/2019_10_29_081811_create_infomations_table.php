<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInfomationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('infomations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 191);
            $table->string('email', 191)->nullable();
            $table->string('phone', 191);
            $table->string('address', 191);
            $table->string('facebook', 255);
            $table->text('content')->nullable();
            $table->text('contents');
            $table->text('content_eng')->nullable();
            $table->text('contents_eng')->nullable();;
            $table->text('image');
            $table->text('logo');   
            $table->string('slug')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('infomations');
    }
}
