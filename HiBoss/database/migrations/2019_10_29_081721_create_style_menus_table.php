<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStyleMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('style_menus', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',191);
            $table->text('description')->nullable();
            $table->string('name_eng',191)->nullable();;
            $table->text('description_eng')->nullable();
            $table->text('image')->nullable();;
            $table->string('slug',191);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('style_menus');
    }
}
