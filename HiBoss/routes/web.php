<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('', 'HomeController')->name('home');
Route::get('ve_chung-toi.html', 'AboutController')->name('about_us');
Route::get('menu.html', 'MenuController')->name('menu_list');
Route::get('hinh-anh/{image}.html', 'GalleryController')->name('gallery');
Route::get('tin-tuc.html', 'BlogController')->name('blog');
Route::get('chi-tiet-tin-tuc/{detail_blog}.html', 'BlogController@detail_blog')->name('detail_blog');
Route::get('tin-tuc-khuyen-mai/{news_promotion}.html', 'BlogController@news_promotion')->name('news_promotion');
Route::get('tin-tuc/{category}.html', 'BlogCategoryController')->name('blogs');
Route::get('/tim-kiem.html', 'BlogController@search_blog')->name('search_blog');
Route::get('lien_he.html', 'ContactController')->name('contact');
Route::post('lien-he.html', 'ContactMessageController')->name('contactMessage');
Route::post('/dat-ban', 'BookTableController')->name('book_table');
Route::get('lang/{locale}', 'LocalizationController@language');
Route::get('menu-list.html', 'MenuListController')->name('menu_list_hi_boss');

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Auth::routes();


